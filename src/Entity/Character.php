<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CharacterRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Controller\GetCharactersCustomAction;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Entity(repositoryClass=CharacterRepository::class)
 * @ApiResource(
 *     normalizationContext={"groups"={"get_collection"}},
 *     denormalizationContext={"groups"={"post", "put"}},
 *     collectionOperations={
 *          "get",
 *          "post",
 *          "get_characters_custom"={
 *             "controller"=GetCharactersCustomAction::class,
 *             "path"="/character",
 *             "method"="GET",
 *             "deserialize"=false,
 *              "openapi_context"={
 *                  "description"="My custom get collection of characters api: you can add query parameters name, status, species, gender, type to filter and page to paginate",
 *     "parameters" = {
 *                      {
 *                          "name" = "name",
 *                          "in" = "query",
 *                          "description" = "The name of the character.",
 *                          "type" : "string"
 *                      },
 *                      {
 *                          "name" = "species",
 *                          "in" = "query",
 *                          "description" = "The species of the character.",
 *                          "type" : "string"
 *                      },
 *                      {
 *                          "name" = "type",
 *                          "in" = "query",
 *                          "description" = "The type or subspecies of the character.",
 *                          "type" : "string"
 *                      },
 *                      {
 *                          "name" = "gender",
 *                          "in" = "query",
 *                          "description" = "The gender of the character ('Female', 'Male', 'Genderless' or 'unknown').",
 *                          "type" : "string"
 *                      },
 *                      {
 *                          "name" = "status",
 *                          "in" = "query",
 *                          "description" = "The status of the character ('Alive', 'Dead' or 'unknown').",
 *                          "type" : "string"
 *                      }
 *                  }
 *              },
 *          }
 *     },
 *     itemOperations={
 *          "put"={"denormalization_context"={"groups"={"put"}}},
 *          "patch",
 *          "delete",
 *          "get"
 *     }
 * )
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="`character`")
 */
class Character
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("get_collection", "post", "character:load")
     */
    public $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups("get_collection", "post", "character:load", "put")
     * @ApiProperty(
     *     attributes={
     *         "openapi_context"={
     *             "type"="string",
     *             "example"="Rick Sanchez"
     *         }
     *     }
     * )
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("get_collection", "post", "character:load", "put")
     * @ApiProperty(
     *     attributes={
     *         "openapi_context"={
     *             "type"="string",
     *             "example"="Alive"
     *         }
     *     }
     * )
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("get_collection", "post", "character:load", "put")
     * @ApiProperty(
     *     attributes={
     *         "openapi_context"={
     *             "type"="string",
     *             "example"="Human"
     *         }
     *     }
     * )
     */
    private $species;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("get_collection", "post", "character:load", "put")
     * @ApiProperty(
     *     attributes={
     *         "openapi_context"={
     *             "type"="string",
     *             "example"="Male"
     *         }
     *     }
     * )
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("get_collection", "post", "character:load", "put")
     * @ApiProperty(
     *     attributes={
     *         "openapi_context"={
     *             "type"="string",
     *             "example"="Genetic experiment"
     *         }
     *     }
     * )
     */
    private $gender;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("get_collection", "post", "character:load", "put")
     * @ApiProperty(
     *     attributes={
     *         "openapi_context"={
     *             "type"="string",
     *             "example"="https://rickandmortyapi.com/api/character/avatar/1.jpeg"
     *         }
     *     }
     * )
     */
    private $image;

    /**
     * @var Episode
     * @ORM\ManyToMany(targetEntity="Episode", cascade={"all"})
     * @ORM\JoinTable(name="characters_episodes",
     *      joinColumns={@ORM\JoinColumn(name="character_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="episode_id", referencedColumnName="id")}
     *      )
     * @Groups("get_collection", "post", "put")     *
     * @ApiProperty(
     *     attributes={
     *         "openapi_context"={
     *             "type"="array",
     *             "example"={"/api/episodes/10", "/api/episodes/11"}
     *         }
     *     }
     * )
     */
    private $episode;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("get_collection", "post", "character:load")
     * @ApiProperty(
     *     attributes={
     *         "openapi_context"={
     *             "type"="string",
     *             "example"="https://rickandmortyapi.com/api/character/1"
     *         }
     *     }
     * )
     */
    private $url;

    /**
     * @ORM\Column(type="datetime")
     * @Groups("get_collection")
     * @ApiProperty(
     *     attributes={
     *         "openapi_context"={
     *             "type"="string",
     *             "example"="2021-09-29 15:16:35"
     *         }
     *     }
     * )
     */
    private $created;

    /**
     * @var Location
     * @ORM\ManyToOne(targetEntity=Location::class)
     * @Groups("get_collection", "post", "put")
     * @ApiProperty(
     *     attributes={
     *         "openapi_context"={
     *             "type"="string",
     *             "example"="/api/locations/10"
     *         }
     *     }
     * )
     */
    private $origin;

    /**
     * @var Location
     * @ORM\ManyToOne(targetEntity=Location::class)
     * @Groups("get_collection", "post", "put")
     * @ApiProperty(
     *     attributes={
     *         "openapi_context"={
     *             "type"="string",
     *             "example"="/api/locations/10"
     *         }
     *     }
     * )
     */
    private $location;

    public function __construct()
    {
        $this->episode = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getSpecies(): ?string
    {
        return $this->species;
    }

    public function setSpecies(?string $species): self
    {
        $this->species = $species;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(?string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getLocation(): ?Location
    {
        return $this->location;
    }

    public function setLocation(?Location $location): self
    {
        $this->location = $location;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     * @return Character
     */
    public function setCreatedValue(): self
    {
        $this->created = new \DateTime();

        return $this;
    }

    /**
     * @return Collection|Episode[]
     */
    public function getEpisode(): Collection
    {
        return $this->episode;
    }

    public function addEpisode(Episode $episode): self
    {
        if (!$this->episode->contains($episode)) {
            $this->episode[] = $episode;
        }

        return $this;
    }

    public function removeEpisode(Episode $episode): self
    {
        $this->episode->removeElement($episode);

        return $this;
    }

    public function getOrigin(): ?Location
    {
        return $this->origin;
    }

    public function setOrigin(?Location $origin): self
    {
        $this->origin = $origin;

        return $this;
    }

    /**
     * @return string
     * @ORM\PostLoad()
     * @Groups({"get_collection"})
     * @SerializedName("internalUrl")
     */
    public function getInternalUr(): string
    {
        return $_ENV['BASE_URL']."characters/".$this->id;
    }
}
