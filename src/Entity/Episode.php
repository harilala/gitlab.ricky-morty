<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\EpisodeRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=EpisodeRepository::class)
 * @ApiResource(
 *     normalizationContext={"groups"={"get_collection"}},
 *     denormalizationContext={"groups"={"post"}},
 *     collectionOperations={},
 *     itemOperations={
 *      "get"
 *     }
 * )
 */
class Episode
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("post")
     */
    public $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("get_collection", "post")
     */
    private $url;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("post")
     */
    private $name;

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
