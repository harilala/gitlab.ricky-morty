<?php
/**
 * Created by PhpStorm.
 * User: harilala
 * Date: 9/29/21
 * Time: 8:53 AM
 */

namespace App\Entity\Constants;


final class Constant
{
    const STATUS_ALIVE = "Alive";
    const STATUS_DEAD = "Dead";
    const STATUS_UNKNOWN = "unknown";

    const GENDER_MALE = "Male";
    const GENDER_FEMALE = "Female";
    const GENDER_GENDERLESS = "Genderless";
    const GENDER_UNKOWN = "unknown";



}