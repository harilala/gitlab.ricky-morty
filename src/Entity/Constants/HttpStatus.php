<?php
/**
 * Created by PhpStorm.
 * User: harilala
 * Date: 9/29/21
 * Time: 3:26 PM
 */

namespace App\Entity\Constants;


final class HttpStatus
{
    const ERROR = 500;

    const FIELD_ERROR = 503;

    const SUCCESS = 200;

    const NOT_FOUND = 404;

    const FORBIDDEN = 403;

    const BAD_REQUEST = 400;

    const DUPLICATED = 402;

}