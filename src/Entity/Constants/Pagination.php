<?php
/**
 * Created by PhpStorm.
 * User: harilala
 * Date: 9/29/21
 * Time: 5:23 PM
 */

namespace App\Entity\Constants;


final class Pagination
{
    public const PAGE_DEFAULT = 1;

    public const PAGE_NB_ITEMS = 20;

    public const PAGE_ITEM_ORDER = 'asc';

    public const PAGE_ITEM_ORDER_COLUMN = 'id';

}