<?php

namespace App\Repository;

use App\Entity\Character;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Character|null find($id, $lockMode = null, $lockVersion = null)
 * @method Character|null findOneBy(array $criteria, array $orderBy = null)
 * @method Character[]    findAll()
 * @method Character[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CharacterRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Character::class);
    }

    // /**
    //  * @return Character[] Returns an array of Character objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Character
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /**
     * @param array $search
     * @return Query|null
     */
    public function findByOrdered($search = []): ?Query
    {
        $qb = $this->createQueryBuilder('c');
            if (!empty($search)) {
                if ( isset($search['name'])) {
                    $qb->andWhere($qb->expr()->like('LOWER(c.name)', '?1'))
                        ->setParameter('1', '%'.strtolower($search['name']).'%');
                }
                if ( isset($search['status'])) {
                    $qb->andWhere($qb->expr()->like('LOWER(c.status)', '?2'))
                        ->setParameter('2', '%' . strtolower($search['status']) . '%');
                }
                if ( isset($search['species'])) {
                    $qb->andWhere($qb->expr()->like('LOWER(c.species)', '?3'))
                        ->setParameter('3', '%' . strtolower($search['species']) . '%');
                }
                if ( isset($search['type'])) {
                    $qb->andWhere($qb->expr()->like('LOWER(c.type)', '?4'))
                        ->setParameter('4', '%' . strtolower($search['type']) . '%');
                }
                if ( isset($search['gender'])) {
                    $qb->andWhere($qb->expr()->like('LOWER(c.gender)', '?5'))
                        ->setParameter('5', '%' . strtolower($search['gender']) . '%');
                }
            }
        $query = $qb->orderBy('c.id', 'ASC')
            ->getQuery();

        return $query;
    }
}
