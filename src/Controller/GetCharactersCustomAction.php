<?php
/**
 * Created by PhpStorm.
 * User: harilala
 * Date: 9/29/21
 * Time: 5:42 PM
 */

namespace App\Controller;


use App\Entity\Character;
use App\Entity\Constants\HttpStatus;
use App\Entity\Constants\Pagination;
use App\Services\PaginatorService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class GetCharactersCustomAction
 * @package App\Controller
 */
class GetCharactersCustomAction extends CommonController
{
    /**
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param PaginatorService $paginatorService
     * @return \Doctrine\ORM\Tools\Pagination\Paginator|JsonResponse
     * @Route(
     *     defaults={
     *          "_api_resource_class"=Character::class
     *     }
     * )
     */
    public function __invoke(Request $request, EntityManagerInterface $em, PaginatorService $paginatorService, ParameterBagInterface $params)
    {
        try {
            $page = (int)$request->query->get('page', Pagination::PAGE_DEFAULT);
            $allParams = $request->query->all();
            $limit = Pagination::PAGE_NB_ITEMS;
            $query = $em->getRepository(Character::class)->findByOrdered($allParams);
            $data = $paginatorService->paginate($query, $page, $limit);
            $length = $paginatorService->total($data);
            $pages = $paginatorService->pageNumber($data);
            $baseUrl = $params->get('base_url');
            $nextUrl = ($page+1 <= (int)$pages &&  $pages > 1) ?
                sprintf($baseUrl."character?page=%s", ($page+1)) : null;
            $previousUrl = ($page-1 > 0) ?
                sprintf($baseUrl."character?page=%s", ($page-1)) : null;

            $dataOutput = [
                'infos' => [
                    'count' => $length,
                    'pages' => $pages,
                    'next' => $nextUrl,
                    'prev' => $previousUrl
                ],
                'results' => $data,
            ];

            return $dataOutput;
        } catch (\Exception $exception) {
            return new JsonResponse([
                "error_msg" => $exception->getMessage(),
                "error_code" => $exception->getCode()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

}