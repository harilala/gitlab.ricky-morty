<?php
/**
 * Created by PhpStorm.
 * User: harilala
 * Date: 9/29/21
 * Time: 5:22 PM
 */

namespace App\Services;


use App\Entity\Constants\Pagination;
use Doctrine\ORM\Tools\Pagination\Paginator;

class PaginatorService
{

    /**
     * @param mixed $query
     * @param int $page
     * @param int|null $limit
     * @return Paginator
     */
    public function paginate(
        $query,
        int $page = Pagination::PAGE_DEFAULT,
        int $limit = null
    ): Paginator {
        $paginator = new Paginator($query);

        if (!empty($limit) && $limit != 0) {
            $firstResult = ($page - 1) * $limit;
            $paginator
                ->getQuery()
                ->setFirstResult($firstResult)
                ->setMaxResults($limit);
        }

        return $paginator;
    }

    /**
     * @param Paginator $paginator
     * @return int
     */
    public function total(Paginator $paginator): int
    {
        return $paginator->count();
    }

    /**
     * @param Paginator $paginator
     * @return bool
     */
    public function currentPageHasNoResult(Paginator $paginator): bool
    {
        return !$paginator->getIterator()->count();
    }

    /**
     * @param Paginator $paginator
     * @return int
     */
    public function pageNumber(Paginator $paginator): int
    {
        $pageNb = ($paginator->count()%Pagination::PAGE_NB_ITEMS > 0) ?
            intdiv($paginator->count(), Pagination::PAGE_NB_ITEMS ) + 1 :
            intdiv($paginator->count(), Pagination::PAGE_NB_ITEMS );

        return $pageNb;
    }

}