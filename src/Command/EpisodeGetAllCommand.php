<?php

namespace App\Command;

use App\Entity\Episode;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class EpisodeGetAllCommand extends Command
{
    protected static $defaultName = 'episode:get-all';
    protected static $defaultDescription = 'Request episode per page. Page number should be 1 or 2 or 3';

    private $httpClient;
    private $em;

    public function __construct($name = null, HttpClientInterface $httpClient, EntityManagerInterface $em)
    {
        parent::__construct($name);
        $this->httpClient = $httpClient;
        $this->em = $em;
    }

    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        try {
            $arg1 = $input->getArgument('arg1');

            if ($arg1) {
                $io->note(sprintf('Page %s requested', $arg1));
            }
            $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
            $normalizer = new ObjectNormalizer($classMetadataFactory);
            $serializer = new Serializer([$normalizer]);

            $url = sprintf(
                "https://rickandmortyapi.com/api/episode/?page=%s",
                $arg1
            );

            $response = $this->httpClient->request(
                'GET',
                $url
            );
            $data = json_decode($response->getContent(), true);
            $objectData = [];
            foreach ($data['results'] as $result) {
                /** @var Episode $episode */
                $episode = $serializer->denormalize(
                    $result,
                    Episode::class,
                    null,
                    ['groups' => ['post']]
                );
                $existingEpisode = $this->em->getRepository(Episode::class)->findOneBy(['name' => $episode->getName()]);
                if (!$existingEpisode) {
                    $this->em->persist($episode);
                }
                $objectData[] = $episode;
                unset($episode);

            }
            $this->em->flush();

            $io->success('Data from the current page saved');

            return 0;
        } catch (\Exception $exception) {
            $io->error('Failed to save current page location list! ');

            return 1;
        }
    }
}
