<?php

namespace App\Command;

use App\Entity\Character;
use App\Entity\Episode;
use App\Entity\Location;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class CharacterGetAllCommand extends Command
{
    protected static $defaultName = 'character:get-all';
    protected static $defaultDescription = 'Request episode per page. Page number should be 1 to 34';

    private $em;
    private $httpClient;

    public function __construct(
        $name = null,
        HttpClientInterface $httpClient,
        EntityManagerInterface $em
    )
    {
        parent::__construct($name);
        $this->httpClient = $httpClient;
        $this->em = $em;
    }

    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        try {
            $arg1 = $input->getArgument('arg1');

            if ($arg1) {
                $io->note(sprintf('You passed an argument: %s', $arg1));
            }

            $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
            $normalizer = new ObjectNormalizer($classMetadataFactory);
            $serializer = new Serializer([$normalizer]);

            $url = sprintf(
                "https://rickandmortyapi.com/api/character/?page=%s",
                $arg1
            );

            $response = $this->httpClient->request(
                'GET',
                $url
            );
            $data = json_decode($response->getContent(), true);
            $objectData = [];
            foreach ($data['results'] as $result) {
                /** @var Character $character */
                $character = $serializer->denormalize(
                    $result,
                    Character::class,
                    null,
                    ['groups' => ['character:load']]
                );

                $originName = $result['origin']['name'] ?? null;
                $locationName = $result['location']['name'] ?? null;
                $episodeArr = $result['episode'] ?? [];
                /** @var Location $origin */
                $origin = $this->em->getRepository(Location::class)->findOneBy(['name' => $originName]);
                /** @var Location $location */
                $location = $this->em->getRepository(Location::class)->findOneBy(['name' => $locationName]);
                $character->setOrigin($origin);
                $character->setLocation($location);
                foreach ($episodeArr as $episodeUrl) {
                    $episode = $this->em->getRepository(Episode::class)->findOneBy(['url' => $episodeUrl]);
                    $character->addEpisode($episode);
                }

                $this->em->persist($character);
                $objectData[] = $character;
                unset($character);
                unset($origin);
                unset($location);
                unset($episode);

            }
            $this->em->flush();

            $io->success('Data of current page saved !.');

            return 0;
        } catch (\Exception $exception) {
            $io->error('Failed to save current page location list! ');
            return 1;
        }
    }
}
