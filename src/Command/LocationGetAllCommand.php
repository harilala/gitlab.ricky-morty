<?php

namespace App\Command;

use App\Entity\Location;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class LocationGetAllCommand extends Command
{
    protected static $defaultName = 'location:get-all';
    protected static $defaultDescription = 'Request episode per page. Page number should be 1 to 6';

    private $em;
    private $httpClient;

    public function __construct($name = null, HttpClientInterface $httpClient, EntityManagerInterface $em)
    {
        parent::__construct($name);
        $this->httpClient = $httpClient;
        $this->em = $em;
    }

    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {

            $arg1 = $input->getArgument('arg1');

            if ($arg1) {
                $io->note(sprintf('You passed an argument: %s', $arg1));
            }

            $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
            $normalizer = new ObjectNormalizer($classMetadataFactory);
            $serializer = new Serializer([$normalizer]);

            $url = sprintf(
                "https://rickandmortyapi.com/api/location/?page=%s",
                $arg1
            );

            $response = $this->httpClient->request(
                'GET',
                $url
            );
            $data = json_decode($response->getContent(), true);
            $objectData = [];
            foreach ($data['results'] as $result) {
                /** @var Location $location */
                $location = $serializer->denormalize(
                    $result,
                    Location::class,
                    null,
                    ['groups' => ['post']]
                );
                $existingLocation = $this->em->getRepository(Location::class)->findOneBy(['name'=>$location->getName()]);
                if (!$existingLocation) {
                    $this->em->persist($location);
                }
                $objectData[] = $location;
                unset($location);

            }
            $this->em->flush();

            $io->success('Save current page location list done! ');

            return 0;
        } catch (\Exception $exception) {
            $io->error('Failed to save current page location list! ');

            return 1;
        }
    }
}
