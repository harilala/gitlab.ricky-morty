<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210929081318 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `character` DROP FOREIGN KEY FK_937AB03456A273CC');
        $this->addSql('CREATE TABLE episode (id INT AUTO_INCREMENT NOT NULL, url VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('DROP TABLE origin');
        $this->addSql('ALTER TABLE `character` DROP FOREIGN KEY FK_937AB03456A273CC');
        $this->addSql('ALTER TABLE `character` ADD episode_id INT DEFAULT NULL, DROP episode');
        $this->addSql('ALTER TABLE `character` ADD CONSTRAINT FK_937AB034362B62A0 FOREIGN KEY (episode_id) REFERENCES episode (id)');
        $this->addSql('ALTER TABLE `character` ADD CONSTRAINT FK_937AB03456A273CC FOREIGN KEY (origin_id) REFERENCES location (id)');
        $this->addSql('CREATE INDEX IDX_937AB034362B62A0 ON `character` (episode_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `character` DROP FOREIGN KEY FK_937AB034362B62A0');
        $this->addSql('CREATE TABLE origin (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, url VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('DROP TABLE episode');
        $this->addSql('ALTER TABLE `character` DROP FOREIGN KEY FK_937AB03456A273CC');
        $this->addSql('DROP INDEX IDX_937AB034362B62A0 ON `character`');
        $this->addSql('ALTER TABLE `character` ADD episode LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:array)\', DROP episode_id');
        $this->addSql('ALTER TABLE `character` ADD CONSTRAINT FK_937AB03456A273CC FOREIGN KEY (origin_id) REFERENCES origin (id)');
    }
}
