# Ricky morty api

provides implementation API as in https://rickandmortyapi.com/documentation/#character

The application is developped with symfony 4.4 / api platform 2.6

The official api platform documentation is available **[on the API Platform website](https://api-platform.com)**.

To run project locally :

Configure **.env** variables 

**BASE_URL**=your root api based url (example 127.0.0.1:8000/api/)

**DATABASE_URL**=your database access url (example postgresql://postgres:mypassword@127.0.0.1:5432/rickymortydatabase?serverVersion=13&charset=utf8)

Run **composer install** in command line tool to install symfony dependances

Run **php bin/console doctrine:database:create** to generate an empty database with database name given in DATABASE_URL

Run **php bin/console doctrine:schema:update --force** to create tables schemas for database as configured in entities

or run **php bin/console make:migration**
then **php bin/console doctrine:migrations:migrate** 

these second option to generate tables schemas need command **composer require --dev symfony/maker-bundle** if maker-bundle not already installed

Run next commands to load data for endpoints tests of the API :

**php bin/console location:get-all 1** 
to load first page data from external api https://rickandmortyapi.com/location=page=1

 the arg 1 in command  refers to page number of results in the api ( values are 1 to 3)

**php bin/console episode:get-all 1**
to load first page data from external api https://rickandmortyapi.com/episode?page=1

 the arg 1 in command  refers to page number of results in the api (page values are 1 to 6)

**php bin/console character:get-all 1**
to load first page data from external api https://rickandmortyapi.com/character?page=1
 
 the arg 1 in command refers to page number of results in the api (page values are 1 to 34 - 671 available characters)

All data are now ready in database

You can run endpoints listed in swagger UI of the application
https://warm-taiga-26478.herokuapp.com/api

Examples :

GET https://warm-taiga-26478.herokuapp.com/api/character

POST https://warm-taiga-26478.herokuapp.com/api/characters

With json body data as the example in swagger section POST /api/characters

PUT https://warm-taiga-26478.herokuapp.com/api/characters/1

With json body data as the example in swagger section PUT /api/characters/{id}

To launch these examples locally, not forget to replace base url of endpoints with your own local base url



